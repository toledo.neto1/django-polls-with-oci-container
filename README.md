# Django-Polls

Django tutorial from [Writing your first Django app](https://docs.djangoproject.com/en/4.0/intro/tutorial01/) in [Django Docs](https://docs.djangoproject.com/en/4.0/).

# OCI (Open Container Initiative) compliant image

This project uses Django Poll's app tho show how a well built Containerfile can reduce the OCI image size. This proccess can be followed through 
Git Versions inside this project. These tips were mostly taken from many articles from [this](https://pythonspeed.com/articles/base-image-python-docker-images/) website.

Inside `containerfile/`, there are three `Containerfile`, each one added in the following Git Version:

* `0.0.1-ociBig`: using the large python:latest (942 MB) image as base image, we got a poll's image of **~1 GB**.

* `0.0.2-ociSmall`: using the python:3.8-slim (129 MB) image as base image, we got a poll's image of **~460 MB**.

* `0.0.3-ociMultistage`: using the python:3.8-slim (129 MB) image as base image **with multistage build**, we got a poll's image of **~129 MB**.

![](./src/images/oci-image-size.jpeg)